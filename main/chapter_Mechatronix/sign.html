


<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Regularised Sign &#8212; PINS v1.4.9
</title>
    <link rel="stylesheet" href="../_static/pygments.css" type="text/css" />
    <link rel="stylesheet" href="../_static/cloud.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../_static/table_styling.css" />
    <link rel="stylesheet" type="text/css" href="../_static/panels-main.c949a650a448cc0ae9fd3441c0e17fb0.css" />
    <link rel="stylesheet" type="text/css" href="../_static/panels-bootstrap.5fd3999ee7762ccc51105388f4a9d115.css" />
    <link rel="stylesheet" type="text/css" href="../_static/panels-variables.06eb56fa6e07937060861dad626602ad.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noticia+Text:400,i,b,bi|Open+Sans:400,i,b,bi|Roboto+Mono:400,i,b,bi&amp;display=swap" type="text/css" />
    
    <script id="documentation_options" data-url_root="../" src="../_static/documentation_options.js"></script>
    <script src="../_static/jquery.js"></script>
    <script src="../_static/underscore.js"></script>
    <script src="../_static/doctools.js"></script>
    <script async="async" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/latest.js?config=TeX-AMS-MML_HTMLorMML"></script>

    
    
     
        <script src="../_static/jquery.cookie.js"></script>
    

    
     
        <script src="../_static/cloud.base.js"></script>
    

    
     
        <script src="../_static/cloud.js"></script>
    

    <link rel="index" title="Index" href="../genindex.html" />
    <link rel="search" title="Search" href="../search.html" />
    <link rel="next" title="Absolute Value" href="abs.html" />
    <link rel="prev" title="Hypot" href="hypot.html" /> 
        <meta name="viewport" content="width=device-width, initial-scale=1">
  </head><body>
    <div class="relbar-top">
        
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="abs.html" title="Absolute Value"
             accesskey="N">next</a> &nbsp; &nbsp;</li>
        <li class="right" >
          <a href="hypot.html" title="Hypot"
             accesskey="P">previous</a> &nbsp; &nbsp;</li>
    <li><a href="../../index.html">PINS</a> &#187;</li>

          <li class="nav-item nav-item-1"><a href="../manual.html" ><strong>PINS</strong> online manual</a> &#187;</li>
          <li class="nav-item nav-item-2"><a href="chapter.html" accesskey="U">Interface of <strong>Mechatronix</strong></a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">Regularised Sign</a></li> 
      </ul>
    </div>
    </div>
  

    <div class="document">
      <div class="documentwrapper">
          <div class="body" role="main">
            
  <div class="section" id="regularised-sign">
<h1>Regularised Sign<a class="headerlink" href="#regularised-sign" title="Permalink to this headline">¶</a>
</h1>
<p>To regularise the sign a typical smooth function used
is the inverse tangent or arctangent.
Even better is the composition of the arctangent with the
sinus. Another good approximation is given by the Error
Function (erf).</p>
<div class="section" id="regularised-sign-with-sinatan">
<h2>Regularised Sign with SinAtan<a class="headerlink" href="#regularised-sign-with-sinatan" title="Permalink to this headline">¶</a>
</h2>
<p>The sign function is approximated with the
following expression:</p>
<div class="math notranslate nohighlight">
\[\textrm{sign}_{\mathrm{sa}}(x):= \sin\arctan(x/\kappa) =
\dfrac{x}{\sqrt{\kappa^2+x^2}}.\]</div>
<p>The parameter <span class="math notranslate nohighlight">\(\kappa\)</span> is selected such that
at <span class="math notranslate nohighlight">\(x=h\)</span> the approximation gives
<span class="math notranslate nohighlight">\(1-\epsilon\)</span>, where <span class="math notranslate nohighlight">\(h\)</span> and
<span class="math notranslate nohighlight">\(\epsilon\)</span> are the tolerances specified
by the user, see Figure <span class="xref std std-ref">XO:fig:sign:SinAtan</span>.</p>
<p>A straightforward computation of the previous requirement yields</p>
<div class="math notranslate nohighlight">
\[\sin\arctan(h/\kappa)= 1-\epsilon \implies \kappa =
h \dfrac{\sqrt{(2-\epsilon)\epsilon}}{1-\epsilon}.\]</div>
<p>Because the function is odd, the same holds for
<span class="math notranslate nohighlight">\(x=-h\)</span> where the approximation becomes
<span class="math notranslate nohighlight">\(\epsilon-1\)</span>.
The first and second derivative of the
<span class="math notranslate nohighlight">\(\textrm{sign}_{\mathrm{sa}}\)</span> function are respectively:</p>
<div class="math notranslate nohighlight">
\[\dfrac{\mathrm{d} }{\mathrm{d}x}\textrm{sign}_{\mathrm{sa}} (x)=
\kappa^2 (\kappa^2+x^2)^{-3/2},\]</div>
<div class="math notranslate nohighlight">
\[\dfrac{\mathrm{d}^2 }{\mathrm{d}x^2}\textrm{sign}_{\mathrm{sa}}(x)=
-3\kappa^2 x (\kappa^2+x^2)^{-5/2}.\]</div>
</div>
<div class="section" id="regularised-sign-with-erf">
<h2>Regularised Sign with erf<a class="headerlink" href="#regularised-sign-with-erf" title="Permalink to this headline">¶</a>
</h2>
<p>A function that closely resembles the sign function
is the Error Function <code class="docutils literal notranslate"><span class="pre">erf</span></code>.
It is scaled to fit the sharpness of the sign with
a parameter <span class="math notranslate nohighlight">\(\kappa\)</span> such that for
<span class="math notranslate nohighlight">\(x=h\)</span> the function <span class="math notranslate nohighlight">\(\mathrm{erf}(h)=1-\epsilon\)</span>,
where <span class="math notranslate nohighlight">\(\epsilon\)</span> and <span class="math notranslate nohighlight">\(h\)</span> are tolerances
set by the user.
In this case it is not possible to give an explicit
expression for <span class="math notranslate nohighlight">\(\kappa\)</span>, but its value is the
numerical result of an easy non linear function,
which is done automatically in <strong>XOptima</strong> and is
invisible for the user. The approximation is</p>
<div class="math notranslate nohighlight">
\[\textrm{sign}_{\mathrm{erf}}(x)
= \mathrm{erf}(\kappa x).\]</div>
<p>Its first and second derivative are</p>
<div class="math notranslate nohighlight">
\[\dfrac{\mathrm{d} }{\mathrm{d}x}\textrm{sign}_{\mathrm{erf}} (x)
=
\frac{2\kappa}{\sqrt{\pi}}\mathrm{e}^{-\kappa^2 x^2},\]</div>
<div class="math notranslate nohighlight">
\[\dfrac{\mathrm{d}^2 }{\mathrm{d}x^2}\textrm{sign}_{\mathrm{erf}}(x)
=
-\frac{4k^3}{\sqrt{\pi}}x \mathrm{e}^{-\kappa^2 x^2}.\]</div>
</div>
</div>



            <div class="clearer"></div>
          </div>
      </div>
    
    
        <div class="sidebar-toggle-group no-js">
            
            <button class="sidebar-toggle" id="sidebar-hide" title="Hide the sidebar menu">
                 «
                <span class="show-for-small">hide menu</span>
                
            </button>
            <button class="sidebar-toggle" id="sidebar-show" title="Show the sidebar menu">
                
                <span class="show-for-small">menu</span>
                <span class="hide-for-small">sidebar</span>
                 »
            </button>
        </div>
    
      <div class="clearer"></div>
    </div>
    <div class="relbar-bottom">
        
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="abs.html" title="Absolute Value"
             >next</a> &nbsp; &nbsp;</li>
        <li class="right" >
          <a href="hypot.html" title="Hypot"
             >previous</a> &nbsp; &nbsp;</li>
    <li><a href="../../index.html">PINS</a> &#187;</li>

          <li class="nav-item nav-item-1"><a href="../manual.html" ><strong>PINS</strong> online manual</a> &#187;</li>
          <li class="nav-item nav-item-2"><a href="chapter.html" >Interface of <strong>Mechatronix</strong></a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">Regularised Sign</a></li> 
      </ul>
    </div>
    </div>

    <div class="footer" role="contentinfo">
        &#169; Copyright 2021, Enrico Bertolazzi and Francesco Biral.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 3.5.4.
    </div>
    <!-- cloud_sptheme 1.4 -->
  </body>
</html>
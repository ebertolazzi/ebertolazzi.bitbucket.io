


<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Absolute Value &#8212; PINS v1.4.9
</title>
    <link rel="stylesheet" href="../_static/pygments.css" type="text/css" />
    <link rel="stylesheet" href="../_static/cloud.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../_static/table_styling.css" />
    <link rel="stylesheet" type="text/css" href="../_static/panels-main.c949a650a448cc0ae9fd3441c0e17fb0.css" />
    <link rel="stylesheet" type="text/css" href="../_static/panels-bootstrap.5fd3999ee7762ccc51105388f4a9d115.css" />
    <link rel="stylesheet" type="text/css" href="../_static/panels-variables.06eb56fa6e07937060861dad626602ad.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noticia+Text:400,i,b,bi|Open+Sans:400,i,b,bi|Roboto+Mono:400,i,b,bi&amp;display=swap" type="text/css" />
    
    <script id="documentation_options" data-url_root="../" src="../_static/documentation_options.js"></script>
    <script src="../_static/jquery.js"></script>
    <script src="../_static/underscore.js"></script>
    <script src="../_static/doctools.js"></script>
    <script async="async" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/latest.js?config=TeX-AMS-MML_HTMLorMML"></script>

    
    
     
        <script src="../_static/jquery.cookie.js"></script>
    

    
     
        <script src="../_static/cloud.base.js"></script>
    

    
     
        <script src="../_static/cloud.js"></script>
    

    <link rel="index" title="Index" href="../genindex.html" />
    <link rel="search" title="Search" href="../search.html" />
    <link rel="next" title="Regularised positive/negative part" href="posneg.html" />
    <link rel="prev" title="Regularised Sign" href="sign.html" /> 
        <meta name="viewport" content="width=device-width, initial-scale=1">
  </head><body>
    <div class="relbar-top">
        
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="posneg.html" title="Regularised positive/negative part"
             accesskey="N">next</a> &nbsp; &nbsp;</li>
        <li class="right" >
          <a href="sign.html" title="Regularised Sign"
             accesskey="P">previous</a> &nbsp; &nbsp;</li>
    <li><a href="../../index.html">PINS</a> &#187;</li>

          <li class="nav-item nav-item-1"><a href="../manual.html" ><strong>PINS</strong> online manual</a> &#187;</li>
          <li class="nav-item nav-item-2"><a href="chapter.html" accesskey="U">Interface of <strong>Mechatronix</strong></a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">Absolute Value</a></li> 
      </ul>
    </div>
    </div>
  

    <div class="document">
      <div class="documentwrapper">
          <div class="body" role="main">
            
  <div class="section" id="absolute-value">
<h1>Absolute Value<a class="headerlink" href="#absolute-value" title="Permalink to this headline">¶</a>
</h1>
<p>The absolute value is one of the most useful functions
that have a discontinuity in the first derivative and
are therefore not available in optimisation programs
based on derivatives. There are three types of approximations
of the absolute value in <strong>XOptima</strong>.</p>
<div class="section" id="absolute-value-regularised-with-erf">
<h2>Absolute value regularised with erf<a class="headerlink" href="#absolute-value-regularised-with-erf" title="Permalink to this headline">¶</a>
</h2>
<p>The <code class="docutils literal notranslate"><span class="pre">erf</span></code> function is very versatile to approximate
those kind of functions. The regularised absolute value
is controlled by the parameter <span class="math notranslate nohighlight">\(h\)</span> so that for
<span class="math notranslate nohighlight">\(x=0\)</span> the function takes the value <span class="math notranslate nohighlight">\(h\)</span>.
The expression of the function is:</p>
<div class="math notranslate nohighlight">
\[|x|_{\mathrm{erf}}:= x\mathrm{erf}(\kappa x)
+ \dfrac{2}{\kappa \sqrt{\pi}}\mathrm{e}^{-\kappa^2 x^2},\]</div>
<p>where <span class="math notranslate nohighlight">\(\kappa:= \dfrac{2}{h\sqrt{\pi}}\)</span> is a
constant once <span class="math notranslate nohighlight">\(h\)</span> is fixed.
The first and second derivative of this function are:</p>
<div class="math notranslate nohighlight">
\[\dfrac{\mathrm{d} }{\mathrm{d}x} |x|_{\mathrm{erf}} =
\mathrm{erf}(\kappa x),\]</div>
<div class="math notranslate nohighlight">
\[\dfrac{\mathrm{d}^2 }{\mathrm{d}x^2}|x|_{\mathrm{erf}} =
\dfrac{2\kappa}{\sqrt{\pi}}\mathrm{e}^{-\kappa^2 x^2}.\]</div>
</div>
<div class="section" id="absolute-value-regularised-with-sinatan">
<h2>Absolute value regularised with SinAtan<a class="headerlink" href="#absolute-value-regularised-with-sinatan" title="Permalink to this headline">¶</a>
</h2>
<p>It is possible to approximate the absolute value with
the function sinus of the arctangent:</p>
<div class="math notranslate nohighlight">
\[|x|_{\mathrm{sa}}:= \frac{x}{\sin\arctan(x/h)} = \sqrt{h^2+x^2}.\]</div>
<p>The shape parameter of this function is <span class="math notranslate nohighlight">\(h\)</span> and
is chosen such that for <span class="math notranslate nohighlight">\(x=0\)</span> the function
<span class="math notranslate nohighlight">\(|x|_{\mathrm{sa}}\)</span> assumes the value <span class="math notranslate nohighlight">\(h\)</span>.
The first and second derivatives are:</p>
<div class="math notranslate nohighlight">
\[\dfrac{\mathrm{d} }{\mathrm{d}x} |x|_{\mathrm{sa}}=
\dfrac{x}{\sqrt{h^2+x^2}},\]</div>
<div class="math notranslate nohighlight">
\[\dfrac{\mathrm{d}^2 }{\mathrm{d}x^2}|x|_{\mathrm{sa}} =
h^2(h^2+x^2)^{-3/2}.\]</div>
</div>
<div class="section" id="absolute-value-regularised-with-polynomials">
<h2>Absolute value regularised with polynomials<a class="headerlink" href="#absolute-value-regularised-with-polynomials" title="Permalink to this headline">¶</a>
</h2>
<p>A third way to approximate the absolute value function
is with a smooth connection of two straight lines with
a polynomial.</p>
</div>
</div>



            <div class="clearer"></div>
          </div>
      </div>
    
    
        <div class="sidebar-toggle-group no-js">
            
            <button class="sidebar-toggle" id="sidebar-hide" title="Hide the sidebar menu">
                 «
                <span class="show-for-small">hide menu</span>
                
            </button>
            <button class="sidebar-toggle" id="sidebar-show" title="Show the sidebar menu">
                
                <span class="show-for-small">menu</span>
                <span class="hide-for-small">sidebar</span>
                 »
            </button>
        </div>
    
      <div class="clearer"></div>
    </div>
    <div class="relbar-bottom">
        
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="posneg.html" title="Regularised positive/negative part"
             >next</a> &nbsp; &nbsp;</li>
        <li class="right" >
          <a href="sign.html" title="Regularised Sign"
             >previous</a> &nbsp; &nbsp;</li>
    <li><a href="../../index.html">PINS</a> &#187;</li>

          <li class="nav-item nav-item-1"><a href="../manual.html" ><strong>PINS</strong> online manual</a> &#187;</li>
          <li class="nav-item nav-item-2"><a href="chapter.html" >Interface of <strong>Mechatronix</strong></a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">Absolute Value</a></li> 
      </ul>
    </div>
    </div>

    <div class="footer" role="contentinfo">
        &#169; Copyright 2021, Enrico Bertolazzi and Francesco Biral.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 3.5.4.
    </div>
    <!-- cloud_sptheme 1.4 -->
  </body>
</html>



<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Numerical Methods &#8212; PINS v1.4.9
</title>
    <link rel="stylesheet" href="../_static/pygments.css" type="text/css" />
    <link rel="stylesheet" href="../_static/cloud.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../_static/table_styling.css" />
    <link rel="stylesheet" type="text/css" href="../_static/panels-main.c949a650a448cc0ae9fd3441c0e17fb0.css" />
    <link rel="stylesheet" type="text/css" href="../_static/panels-bootstrap.5fd3999ee7762ccc51105388f4a9d115.css" />
    <link rel="stylesheet" type="text/css" href="../_static/panels-variables.06eb56fa6e07937060861dad626602ad.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noticia+Text:400,i,b,bi|Open+Sans:400,i,b,bi|Roboto+Mono:400,i,b,bi&amp;display=swap" type="text/css" />
    
    <script id="documentation_options" data-url_root="../" src="../_static/documentation_options.js"></script>
    <script src="../_static/jquery.js"></script>
    <script src="../_static/underscore.js"></script>
    <script src="../_static/doctools.js"></script>
    <script async="async" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/latest.js?config=TeX-AMS-MML_HTMLorMML"></script>

    
    
     
        <script src="../_static/jquery.cookie.js"></script>
    

    
     
        <script src="../_static/cloud.base.js"></script>
    

    
     
        <script src="../_static/cloud.js"></script>
    

    <link rel="index" title="Index" href="../genindex.html" />
    <link rel="search" title="Search" href="../search.html" />
    <link rel="next" title="PINS installation" href="installation.html" />
    <link rel="prev" title="Optimal Control" href="OCP.html" /> 
        <meta name="viewport" content="width=device-width, initial-scale=1">
  </head><body>
    <div class="relbar-top">
        
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="installation.html" title="PINS installation"
             accesskey="N">next</a> &nbsp; &nbsp;</li>
        <li class="right" >
          <a href="OCP.html" title="Optimal Control"
             accesskey="P">previous</a> &nbsp; &nbsp;</li>
    <li><a href="../../index.html">PINS</a> &#187;</li>

          <li class="nav-item nav-item-1"><a href="../manual.html" ><strong>PINS</strong> online manual</a> &#187;</li>
          <li class="nav-item nav-item-2"><a href="chapter.html" accesskey="U"><strong>PINS</strong> manual</a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">Numerical Methods</a></li> 
      </ul>
    </div>
    </div>
  

    <div class="document">
      <div class="documentwrapper">
          <div class="body" role="main">
            
  <div class="section" id="numerical-methods">
<span id="chap-num"></span><h1>Numerical Methods<a class="headerlink" href="#numerical-methods" title="Permalink to this headline">¶</a>
</h1>
<p>The indirect methods are based on the classic theory of calculus of
variations and on the famous Pontryagin’s Maximum (Minimum) Principle (PMP).
Starting from the necessary first order optimality conditions they obtain
a two-point (in general a multi-point) boundary value problem.</p>
<p>It is derived from the first variation of the Lagrangian function
associated to the optimal control problem.
An equivalent derivation is possible taking derivatives of
the Hamiltonian function.</p>
<p>The boundary conditions of this BV problem are given by the initial/final
condition given by the problem itself, other are yielded from the
transversal condition of the adjoint variables. Of course, by the intrinsic
nature of the optimal control problems, a closed form analytical solution
is seldom obtained, but the indirect methods can produce it.
In presence of path constraint or inequalities it is difficult to apply
the PMP to solve for an explicit formula for the control,
this leads to state dependent switches.
The claimed disadvantage of the indirect method is that the resulting
BV problems are difficult to solve.
This is not completely true, because today there are various techniques
to solve systems of differential equations.
It is also mandatory to analyse the computed solution, because it is only
extremal but not necessary a minimum.
This can be accomplished inspecting the problem
(convexity, second variation, etc).
The advantages are given by the underlying philosophy of
<em>first optimize, then discretize</em>: the boundary value problem has dimension
<span class="math notranslate nohighlight">\(2\times n_x\)</span> where <span class="math notranslate nohighlight">\(n_x\)</span> is the number of state variables,
therefore even large scale systems are feasible.</p>
<p>A different approach to OCPs is given by the direct methods which follow
the philosophy of <em>first discretize, then optimize</em> and are somehow the
opposite of the indirect methods.
Here the state and the control variables are approximated by polynomial
interpolation, the target functional itself is approximated by a cost function.
Hence the problem is discretized on a mesh, and the optimization variables
become the unknowns of a general nonlinear programming problem.
There are three main algorithms employed in the application of a direct
method, the first is the shooting method (single and multiple) which
results in small NLP problems; the second is the pseudospectral method
(medium sized problem); the third is the collocation method, which is
the most accurate at the price of a very large NLP.
The main advantage of the direct methods is that NLPs are widely
studied and a plethora of state of art solution algorithms are available.
Moreover it is easier to treat inequality constraints because they have
their natural equivalent form in the associated NLP problem.
The principal disadvantage is that direct methods produce only suboptimal
or approximate solutions.
Nowadays they are very popular because they are easy to understand
and apply (no calculus of variations needed), they are also robust.</p>
<p>The third family of numerical methods to solve an optimal control problem
is given by algorithms that make use of the Hamilton-Jacobi-Bellman equation.
The idea behind this algorithms is the Principle of Optimality,
which states that any subarc of an optimal trajectory is also optimal.
A grid <span class="math notranslate nohighlight">\(a=\zeta_0&lt;\ldots&lt;\zeta_N=b\)</span> is introduced over the time
interval <span class="math notranslate nohighlight">\([a,b]\)</span>, and by the principle of optimality,
on each subinterval <span class="math notranslate nohighlight">\([\zeta_k,\zeta_{k+1}]\)</span> the restriction of the
functional on that interval is optimized.
The resulting partial differential equation is solved recursively backwards
starting at the end of the time interval.
Advantages of this method are that it searches the whole state space
giving a global optimum, can have optimal feedback controls precomputed,
admits some analytical solutions (for linear systems with quadratic cost),
the so called <em>viscosity solutions</em> exist and are feasible for a quite
general class of nonlinear problems.
The main disadvantage of Dynamic Programming is that the resulting
partial differential equation is in a high dimensional space and is
in general non tractable.
This is what Bellman called the <em>curse of dimensionality</em>.</p>
<div class="section" id="ocp-formulation">
<h2>OCP formulation<a class="headerlink" href="#ocp-formulation" title="Permalink to this headline">¶</a>
</h2>
</div>
<div class="section" id="discretisation">
<h2>Discretisation<a class="headerlink" href="#discretisation" title="Permalink to this headline">¶</a>
</h2>
</div>
<div class="section" id="nonlinear-solver">
<h2>Nonlinear solver<a class="headerlink" href="#nonlinear-solver" title="Permalink to this headline">¶</a>
</h2>
</div>
</div>



            <div class="clearer"></div>
          </div>
      </div>
    
    
        <div class="sidebar-toggle-group no-js">
            
            <button class="sidebar-toggle" id="sidebar-hide" title="Hide the sidebar menu">
                 «
                <span class="show-for-small">hide menu</span>
                
            </button>
            <button class="sidebar-toggle" id="sidebar-show" title="Show the sidebar menu">
                
                <span class="show-for-small">menu</span>
                <span class="hide-for-small">sidebar</span>
                 »
            </button>
        </div>
    
      <div class="clearer"></div>
    </div>
    <div class="relbar-bottom">
        
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="installation.html" title="PINS installation"
             >next</a> &nbsp; &nbsp;</li>
        <li class="right" >
          <a href="OCP.html" title="Optimal Control"
             >previous</a> &nbsp; &nbsp;</li>
    <li><a href="../../index.html">PINS</a> &#187;</li>

          <li class="nav-item nav-item-1"><a href="../manual.html" ><strong>PINS</strong> online manual</a> &#187;</li>
          <li class="nav-item nav-item-2"><a href="chapter.html" ><strong>PINS</strong> manual</a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">Numerical Methods</a></li> 
      </ul>
    </div>
    </div>

    <div class="footer" role="contentinfo">
        &#169; Copyright 2021, Enrico Bertolazzi and Francesco Biral.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 3.5.4.
    </div>
    <!-- cloud_sptheme 1.4 -->
  </body>
</html>